  # Tangible AI Internship Notes
  ## notes
  
- review bash terminal commands 
   - cd:"Change Current Working Directory." You may want to change the current working directory, so that you can access different subdirectories and files.
   (cd ..)means that you can go back to the parent directory of any current directory by using the command cd .., as the full path of the current working directory is understood by Bash.
   
   -mkdir: "Create a New Directory." The first step in creating a new directory is to navigate to the directory that you would like to be the parent directory to this new directory using cd. Then, use the command mkdir followed by the name you would like to give the new directory (e.g. mkdir directory-name).
   
   - ls:"Print a List of Files and Subdirections." To see a list of all subdirectories and files within your current working directory, use the command ls.
   
   - cat: "Concatenate." It reads data from the file and gives their content as output. It helps us to create, view, concatenate files
   - pwd:"Print Current Working Directory." Your current working directory is the directory where your commands are being executed. It is typically printed as the full path to the directory (meaning that you can see the parent directory).
To print the name of the current working directory, use the command pwd.

   - rm: "Delete a File." To delete a specific file, you can use the command rm followed by the name of the file you want to delete (e.g. rm filename). For example, you can delete the addresses.txt file under the home directory.

   - mv: ` man mv` 
   - cp: `man cp`




- review git commands 
   - clone: Git clone is primarily used to point to an existing repo and make a clone or copy of that repo at in a new directory, at another location. The original repository can be located on the local filesystem or on remote machine accessible supported protocols. The git clone command copies an existing Git repository.
   
   - pull: The git pull command is used to fetch and download content from a remote repository and immediately update the local repository to match that content. Merging remote upstream changes into your local repository is a common task in Git-based collaboration work flows.
   
   - push: To add the changes to your git repo files on your computer to the version of your repository on GitLab, you need to push them GitLab.
   
   - commit: Git commit command takes a snapshot representing the staged changes.After running the Git commit command, you need to type in the description of the commit in the text editor.This Git commit example shows how you set the description with the commit function:

                git commit -m "<message>"

The following example shows how to save a snapshot of changes done in the whole working directory. This code only works for tracked files.

                 git commit -a
    
   - status: The git status command displays the state of the working directory and the staging area. It lets you see which changes have been staged, which haven't, and which files aren't being tracked by Git.
    
   - log: The Git Log tool allows you to view information about previous commits that have occurred in a project. The simplest version of the log command shows the commits that lead up to the state of the currently checked out branch. These commits are shown in reverse chronological order (the most recent commits first). 
 
## sublime

1. add folder 
2. save project as 
3. close sublime
4. reopen sublime
5. open recent project



  
  
