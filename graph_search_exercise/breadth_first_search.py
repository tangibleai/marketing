N=5

INFINITE=9999999999

def breadth_first_search(residual_graph=[[]], source=0, sink=0, parent_tracker=[]):
    print("breadth_first_search")
    print(residual_graph) 
    print(parent_tracker)
    print(source) 
    print()



    queue=[]
    visited=[]

    for x in range(N):
        visited.append(False)
    queue.append(source)
    visited[source]=True
    parent_tracker[source]=-1


    while len(queue)>0:
        u= queue.pop(0)
        for v in range(0,N):
            if visited[v]== False and residual_graph[u][v]>0:
            
                queue.append(v)
                visited[v]=True
                parent_tracker[v]=u
    return False
    if visited[sink]:
        return True
    else:
        return False

def algorithm(graph,source,sink):
    N=5
    parent_tracker=[0]*5# length should be 5.
    visited=[]
    queue=[]
    source=0
    sink=0
    INFINITE=9999999999 
    u,v=0,0
   
    maxFlow=0
    
    while True:
        if not breadth_first_search(residual_graph=graph,source=source,sink=sink,parent_tracker=parent_tracker):
           break 
        print("Inside While Loop")
         
        print(residual_graph) 
        print(parent_tracker)
        print(source) 
        print()

        path_flow= INFINITE
        v = sink
        
        while not v== source:
        
            u = parent_tracker[v]
            path_flow = min(path_flow, residual_graph[u][v])
            v= parent_tracker[v]
            
        v = sink
        
        while not v==source:
        
            u = parent_tracker[v]
            residual_graph[u][v] -= path_flow
            residual_graph[v][u]+= path_flow
            v = parent_tracker[v]
        maxFlow += path_flow
    
    return maxFlow


def main():
   
    graph= [[0,6,0,0,5],
           [6,0,4,7,3],
           [0,4,0,8,0],  
           [0,7,8,0,5],
           [5,3,0,5,0]]
    N=len(graph)   
    parent_tracker=[0]*N # length should be 5.

    #for x in range(0,N):
        #parent_tracker.append(0)
    print("The max possible flow is{}".format(algorithm(graph,0,N-1)))    
   
    #breadth_first_search(residual_graph=graph ,source=1,sink=5, parent_tracker=[])

if __name__ == "__main__":  
    main()
    
    
    
    
    
    
                