N=5

INFINITE=9999999999

def BreadthFirstSearch(residual_graph=[[]], source=0, sink=0, parentTracker=[]):
    queue=[]
    visited=[]
    for x in range(N):
        visited.append(False)
    queue.append(source)
    visited[source]=True
    parentTracker[source]=-1
    
    while len(queue)>0:
        u= queue.pop(0)
        for v in range(0,N):
            if visited[v]== False and residual_graph[u][v]>0:
            
                queue.append(v)
                visited[v]=True
                parentTracker[v]=u
    if visited[sink]:
        return True
    else:
        return False

def Algorithm(graph,source,sink):
    
    u,v=0,0
    residual_graph= graph
    maxFlow=0
    
    while BreadthFirstSearch(residual_graph,source,sink,parentTracker):
        
        pathFlow= INFINITE
        v = sink
        
        while not v== source:
        
            u = parentTracker[v]
            pathFlow = min(pathFlow, residual_graph[u][v])
            v= parentTracker[v]
            
        v = sink
        
        while not v==source:
        
            u = parentTracker[v]
            residual_graph[u][v] -= pathFlow
            residual_graph[v][u]+= pathFlow
            v = parentTracker[v]
        maxFlow += pathFlow
    return maxFlow
def main():
   
    graph= [[0,6,0,0,5],
           [6,0,4,7,3],
           [0,4,0,8,0],  
           [0,7,8,0,5],
           [5,3,0,5,0]]
            
         
    for x in range(0,N):
        parentTracker.append(0)
    print("The max possible flow is{}".format(Algorithm(graph,0,N-1)))    
    
    BreadthFirstSearch(residual_graph=graph ,source=0,sink=5, parentTracker=[])

    
    
    
    
    
    
    
    
                